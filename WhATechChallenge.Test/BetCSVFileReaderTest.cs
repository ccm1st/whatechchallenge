﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WHATechChallenge.Main.Common;

namespace WhATechChallenge.Test
{
    [TestFixture]
    public class BetCSVFileReaderTest
    {
        [Test]
        public void Proper_File_Return_All_Bets()
        {
            string filePath = "TestResource/two bets.csv";
            
            BetCSVFileReader reader = new BetCSVFileReader(filePath, ',');
            var result = reader.ParseBetDetails();
            
            Assert.AreEqual(2, result.Count);
        }

        [Test]
        public void File_With_Invalid_Row_Return_Valid_Bets()
        {
            string filePath = "TestResource/three valid bets.csv";

            BetCSVFileReader reader = new BetCSVFileReader(filePath, ',');
            var result = reader.ParseBetDetails();

            Assert.AreEqual(3, result.Count);
        }

        [Test]
        public void Invalid_File_Test_Return_Empty_List()
        {
            string filePath = "randomasdfadsf";
            
            BetCSVFileReader reader = new BetCSVFileReader(filePath, ',');
            var result = reader.ParseBetDetails();

            Assert.IsEmpty(result);
        }

    }
}
