﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WHATechChallenge.Main.BettingBiz;
using WHATechChallenge.Main.Model;

namespace WhATechChallenge.Test.TestHelper
{
    public class BetAnalyserWithMockedStats :BetAnalyser
    {
        public BetAnalyserWithMockedStats(IBettingConfig config, List<BetDetail> lsSettledBets, List<BetDetail> lsUnsettledBets) 
            : base(config, lsSettledBets, lsUnsettledBets)
        {
            
        }

        public void MockCustomerStats(List<CustomerStats> lsList)
        {
            base._lsCustomers = lsList;
        }
    }
}
