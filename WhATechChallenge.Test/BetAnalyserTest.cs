﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WHATechChallenge.Main.BettingBiz;
using WHATechChallenge.Main.Model;
using WhATechChallenge.Test.TestHelper;

namespace WhATechChallenge.Test
{
    [TestFixture]
    public class BetAnalyserTest
    {
        [Test]
        public void Test_AnalyseBetHistory_Three_Customer_Bets_Return_Three_Customer_Stats()
        {
            var lsBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 5, WinAmount = 10},
                new BetDetail{ CustomerId = 2, Stake = 15, WinAmount = 210},
                new BetDetail{ CustomerId = 3, Stake = 25, WinAmount = 310},
                new BetDetail{ CustomerId = 3, Stake = 25, WinAmount = 310},
            };

            var config= new Mock<IBettingConfig>();
            var analyser = new BetAnalyser(config.Object, lsBets, null);

            List<CustomerStats> stats= analyser.AnalyseBetHistory();

            Assert.AreEqual(stats.Count, 3);
            Assert.AreEqual(stats[0].CustomerId, 1);
            Assert.AreEqual(stats[1].CustomerId, 2);
            Assert.AreEqual(stats[2].CustomerId, 3);
        }

        [Test]
        public void Test_AnalyseBetHistory_Customer_Winning_Percentage_With_Two_Bets()
        {
            var lsBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 5, WinAmount = 10},
                new BetDetail{ CustomerId = 1, Stake = 15, WinAmount = 0},
            };

            var config = new Mock<IBettingConfig>();
            var analyser = new BetAnalyser(config.Object, lsBets, null);

            List<CustomerStats> stats = analyser.AnalyseBetHistory();
            
            Assert.AreEqual(stats.Count, 1);
            Assert.AreEqual(stats[0].AverageBet, (lsBets[0].Stake + lsBets[1].Stake) / 2);
            Assert.AreEqual(stats[0].WinningPercentage, 50);
        }

        [Test]
        public void Test_AnalyseBetHistory_Customer_Average_With_Two_Bets()
        {
            var lsBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 10},
                new BetDetail{ CustomerId = 1, Stake = 0},
            };

            var config = new Mock<IBettingConfig>();
            var analyser = new BetAnalyser(config.Object, lsBets, null);

            List<CustomerStats> stats = analyser.AnalyseBetHistory();

            Assert.AreEqual(stats.Count, 1);
            Assert.AreEqual(stats[0].AverageBet, 5);
        }

        [Test]
        public void Test_AnalyseBetHistory_Empty_Bets_Return_Empty_List()
        {
            var lsBets = new List<BetDetail>();
            var config = new Mock<IBettingConfig>();
            var analyser = new BetAnalyser(config.Object, lsBets, null);

            List<CustomerStats> stats = analyser.AnalyseBetHistory();

            Assert.AreEqual(stats.Count, 0);
        }

        [Test]
        public void Test_AnalyseBetHistory_Null_Bets_Return_Empty_List()
        {
            var config = new Mock<IBettingConfig>();
            var analyser = new BetAnalyser(config.Object, null, null);

            List<CustomerStats> stats = analyser.AnalyseBetHistory();

            Assert.AreEqual(stats.Count, 0);
        }

        [Test]
        public void Test_GetHighWinningRateCustomers_Return_Two_Customer()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetWinningPercentageThrehold()).Returns(60);

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, null);
            analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 1, WinningPercentage = 80},
                new CustomerStats { CustomerId = 2, WinningPercentage = 61},
                new CustomerStats { CustomerId = 3, WinningPercentage = 60},
            });

            var result = analyser.GetHighWinningRateCustomers();
            Assert.AreEqual(result.Count, 2);
        }

        [Test]
        public void Test_GetHighWinningRateCustomers_With_Zero_Customer_Return_Empty_List()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetWinningPercentageThrehold()).Returns(60);

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, null);
            analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 1, WinningPercentage = 40},
                new CustomerStats { CustomerId = 2, WinningPercentage = 0},
                new CustomerStats { CustomerId = 3, WinningPercentage = 60},
            });

            var result = analyser.GetHighWinningRateCustomers();
            Assert.AreEqual(result.Count, 0);
        }

        [Test]
        public void Test_GetHighRiskBets_Return_Three_Bets()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetWinningPercentageThrehold()).Returns(60);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 5, WinAmount = 10},
                //high risk bets
                new BetDetail{ CustomerId = 2, Stake = 5, WinAmount = 10},
                new BetDetail{ CustomerId = 3, Stake = 5, WinAmount = 30},
                new BetDetail{ CustomerId = 3, Stake = 5, WinAmount = 40},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
             analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 1, WinningPercentage = 40},
                new CustomerStats { CustomerId = 2, WinningPercentage = 70},
                new CustomerStats { CustomerId = 3, WinningPercentage = 80},
            });
            
            var result= analyser.GetHighRiskBets();

            Assert.AreEqual(result.Count, 3);
            Assert.AreEqual(result[0].CustomerId, 2);
            Assert.AreEqual(result[1].CustomerId, 3);
            Assert.AreEqual(result[1].WinAmount, 30);
            Assert.AreEqual(result[2].CustomerId, 3);
            Assert.AreEqual(result[2].WinAmount, 40);
        }

        [Test]
        public void Test_GetHighRiskBets_Return_Zero_Bets()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetWinningPercentageThrehold()).Returns(60);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 5, WinAmount = 10},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
            analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 1, WinningPercentage = 40},
                new CustomerStats { CustomerId = 2, WinningPercentage = 70},
            });

            var result = analyser.GetHighRiskBets();

            Assert.AreEqual(result.Count, 0);
        }

        [Test]
        public void Test_GetUnusualBets_With_Single_Customer_Return_Two_Bets()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetUnusalStakeMultiplier()).Returns(2);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 20},
                new BetDetail{ CustomerId = 1, Stake = 19},
                new BetDetail{ CustomerId = 1, Stake = 21},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
            analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 1, AverageBet = 10},
            });

            var result = analyser.GetUnusualBets();

            Assert.AreEqual(1, result.Count);
            Assert.That(result[0].Stake == 21);
        }

        [Test]
        public void Test_GetUnusualBets_With_Multiple_Customer_History_Return_Two_Bets()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetUnusalStakeMultiplier()).Returns(2);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 21},
                new BetDetail{ CustomerId = 2, Stake = 20},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
            analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 2, AverageBet = 10},
                new CustomerStats { CustomerId = 1, AverageBet = 9},
            });

            var result = analyser.GetUnusualBets();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].CustomerId);
        }


        [Test]
        public void Test_GetUnusualBets_With_No_Customer_History_Return_Empty_List()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetUnusalStakeMultiplier()).Returns(2);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 20},
                new BetDetail{ CustomerId = 1, Stake = 19},
                new BetDetail{ CustomerId = 1, Stake = 21},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
            var result = analyser.GetUnusualBets();
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Test_GetHighlyUnusualBets_Single_Customer_Return_Two_Bets()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetHighRiskStakeMultiplier()).Returns(2);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 20},
                new BetDetail{ CustomerId = 1, Stake = 19},
                new BetDetail{ CustomerId = 1, Stake = 21},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
            analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 1, AverageBet = 10},
            });

            var result = analyser.GetHighlyUnusualBets();

            Assert.AreEqual(1, result.Count);
            Assert.That(result[0].Stake == 21);
        }

        [Test]
        public void Test_GetHighlyUnusualBets_With_Multiple_Customer_History_Return_Two_Bets()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetHighRiskStakeMultiplier()).Returns(2);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 21},
                new BetDetail{ CustomerId = 2, Stake = 20},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
            analyser.MockCustomerStats(new List<CustomerStats>
            {
                new CustomerStats { CustomerId = 2, AverageBet = 10},
                new CustomerStats { CustomerId = 1, AverageBet = 9},
            });

            var result = analyser.GetHighlyUnusualBets();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].CustomerId);
        }

        [Test]
        public void Test_GetHighlyUnusualBets_With_No_Customer_History_Return_Empty_List()
        {
            var mockConfig = new Mock<IBettingConfig>();
            mockConfig.Setup(a => a.GetHighRiskStakeMultiplier()).Returns(2);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, Stake = 20},
                new BetDetail{ CustomerId = 1, Stake = 19},
                new BetDetail{ CustomerId = 1, Stake = 21},
            };

            var analyser = new BetAnalyserWithMockedStats(mockConfig.Object, null, lsUnsettledBets);
            var result = analyser.GetHighlyUnusualBets();
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        [TestCase(1000, 999, Result = 0)]
        [TestCase(1000, 1000, Result = 1)]
        [TestCase(1000, 1001, Result = 1)]
        public int Test_GetBetsOverThreshold_With_Single_Customer(int threhold, int winAmount)
        {
            var mockConfig = new Mock<IBettingConfig>();
            
            mockConfig.Setup(a => a.GetWinThreshold()).Returns(threhold);

            var lsUnsettledBets = new List<BetDetail>
            {
                new BetDetail{ CustomerId = 1, WinAmount = winAmount},
            };

            var analyser = new BetAnalyser(mockConfig.Object, null, lsUnsettledBets);
            var result = analyser.GetBetsOverThreshold();
            return result.Count;
        }
    }
}


