﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WHATechChallenge.Main.Model
{
    public class CustomerStats
    {
        public int CustomerId { get; set; }
        public double WinningPercentage { get; set; } 
        public decimal AverageBet { get; set; }
    }
}
