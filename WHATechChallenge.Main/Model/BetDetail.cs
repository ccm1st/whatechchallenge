﻿namespace WHATechChallenge.Main.Model
{
    public class BetDetail
    {
        public int CustomerId { get; set; }
        public int EventId { get; set; }
        public int ParticipationId { get; set; }
        public decimal Stake { get; set; }
        public decimal WinAmount { get; set; }
    }
}
