﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WHATechChallenge.Main.Model
{
    public interface IBettingConfig
    {
        int GetWinningPercentageThrehold();

        int GetUnusalStakeMultiplier();

        int GetHighRiskStakeMultiplier();

        decimal GetWinThreshold();
    }

    public class BettingConfig : IBettingConfig
    {
        public int GetWinningPercentageThrehold()
        {
            return 60;
        }

        public int GetUnusalStakeMultiplier()
        {
            return 10;
        }

        public int GetHighRiskStakeMultiplier()
        {
            return 30;
        }

        public decimal GetWinThreshold()
        {
            return 1000;
        }
    }
}
