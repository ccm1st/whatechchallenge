﻿using System;
using System.Collections.Generic;
using System.Linq;
using WHATechChallenge.Main.BettingBiz;
using WHATechChallenge.Main.Common;
using WHATechChallenge.Main.Model;

namespace WHATechChallenge.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            string settledFile = "Data/Settled.csv";
            string unsettledFile = "Data/Unsettled.csv";
            char delmitor = ',';

            IBetFileReader parser = new BetCSVFileReader(settledFile, delmitor);
            var lsSettledBets = parser.ParseBetDetails();

            parser = new BetCSVFileReader(unsettledFile, delmitor);
            var lsUnsettledBets = parser.ParseBetDetails();

            IBettingConfig config = new BettingConfig();
            BetAnalyser analyser = new BetAnalyser(config, lsSettledBets, lsUnsettledBets);
            var lsStats= analyser.AnalyseBetHistory();
            List<CustomerStats> lsHighWinningRateCustomers = analyser.GetHighWinningRateCustomers();

            Console.WriteLine("Customer Stats");
            foreach (var customer in lsStats)
            {
                Console.WriteLine("customer id : {0}  winning percentage: {1}, average: {2}", customer.CustomerId, customer.WinningPercentage, customer.AverageBet);
            }

            Console.WriteLine("****************************High Winner Rate customers****************************");
            foreach (var customer in lsHighWinningRateCustomers)
            {
                Console.WriteLine("customer id : {0}  winning percentage: {1}",customer.CustomerId, customer.WinningPercentage);
            }

            var lsHighRisk = lsUnsettledBets.Where(a => lsHighWinningRateCustomers.Exists(hr => hr.CustomerId == a.CustomerId));
            Console.WriteLine("****************************High Risk Bets****************************");
            foreach (var customer in lsHighRisk)
            {
                Console.WriteLine("customer id : {0}  win amount: {1}", customer.CustomerId, customer.WinAmount);
            }

            var lsUnusal = analyser.GetUnusualBets();
            Console.WriteLine("****************************Unsual Bets****************************");
            foreach (var customer in lsUnusal)
            {
                Console.WriteLine("customer id : {0}  win amount: {1}  stake: {2} ", customer.CustomerId, customer.WinAmount, customer.Stake);
            }

            var lsHighlyUnusual = analyser.GetHighlyUnusualBets();

            Console.WriteLine("****************************Higly Unusal Bets****************************");
            foreach (var customer in lsHighlyUnusual)
            {
                Console.WriteLine("customer id : {0}  win amount: {1}  stake: {2} ", customer.CustomerId, customer.WinAmount, customer.Stake);
            }


            var lsOverWonThreshold = analyser.GetBetsOverThreshold();
            Console.WriteLine("****************************Bets over threshold****************************");
            foreach (var customer in lsOverWonThreshold)
            {
                Console.WriteLine("customer id : {0}  win amount: {1}", customer.CustomerId, customer.WinAmount);
            }
            
            Console.ReadLine();
        }
    }
}
