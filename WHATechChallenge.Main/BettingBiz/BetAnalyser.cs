﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WHATechChallenge.Main.Model;

namespace WHATechChallenge.Main.BettingBiz
{
    public class BetAnalyser
    {
        private readonly IBettingConfig _config;
        private readonly List<BetDetail> _lsSettledBets;
        private readonly List<BetDetail> _lsUnsettledBets;
        protected List<CustomerStats> _lsCustomers;

        public BetAnalyser(IBettingConfig config, List<BetDetail> lsSettledBets, List<BetDetail> lsUnsettledBets)
        {
            _config = config;
            _lsSettledBets = lsSettledBets;
            _lsUnsettledBets = lsUnsettledBets;
            _lsCustomers = new List<CustomerStats>();
        }

        /// <summary>
        /// This method needs to be called in order for any of the other risk analysis function to work
        /// </summary>
        /// <returns></returns>
        public virtual List<CustomerStats> AnalyseBetHistory()
        {
            if (_lsSettledBets == null)
            {
                return new List<CustomerStats>();
            }

            _lsCustomers = _lsSettledBets.GroupBy(a => a.CustomerId).Select(a => new CustomerStats
            {
                CustomerId = a.Key,
                WinningPercentage = GetWinningPercentage(a.ToList()),
                AverageBet = GetAverageBet(a.ToList())
            }).ToList();
            return _lsCustomers;
        }


        public List<CustomerStats> GetHighWinningRateCustomers()
        {
            return _lsCustomers.Where(a => a.WinningPercentage > _config.GetWinningPercentageThrehold()).ToList();
        }

        public List<BetDetail> GetHighRiskBets()
        {
            var lsHighWinningRateCustomers = GetHighWinningRateCustomers();
            return _lsUnsettledBets.Where(a => lsHighWinningRateCustomers.Exists(hr => hr.CustomerId == a.CustomerId)).ToList();
        }

        /// <summary>
        /// Bets where the stake is more than the x times higher than that customer’s average bet in their betting history, where x is defined as UnusalStakeMultiplier in the config file
        /// </summary>
        public List<BetDetail> GetUnusualBets()
        {
            return GetBetsWithAverageOver(_config.GetUnusalStakeMultiplier());
        }

        /// <summary>
        /// Bets where the stake is more than the x times higher than that customer’s average bet in their betting history, where x is defined in as HighRiskStakeMultiplier in the betting config
        /// </summary>
        public List<BetDetail> GetHighlyUnusualBets()
        {
            return GetBetsWithAverageOver(_config.GetHighRiskStakeMultiplier());
        }

        /// <summary>
        /// Bets where the amount to be won is x or more, where x is defined as WonThreshold in the betting config  . 
        /// </summary>
        public List<BetDetail> GetBetsOverThreshold()
        {
            return _lsUnsettledBets.Where(a => a.WinAmount >= _config.GetWinThreshold()).ToList();
        }

        private List<BetDetail> GetBetsWithAverageOver(int multipler)
        {
            var lsBets = new List<BetDetail>();

            _lsUnsettledBets.ForEach(bet =>
            {
                CustomerStats stats = _lsCustomers.SingleOrDefault(cust => cust.CustomerId == bet.CustomerId);
                if (stats != null && stats.AverageBet > 0)
                {
                    if (bet.Stake / stats.AverageBet > multipler)
                    {
                        lsBets.Add(bet);
                    }
                }
            });

            return lsBets;
        }

        private decimal GetAverageBet(List<BetDetail> lsBets)
        {
            if (lsBets.Count == 0)
            {
                return 0;
            }

            return lsBets.Select(a => a.Stake).Average();
        }

        private double GetWinningPercentage(List<BetDetail> lsBets)
        {
            if (lsBets.Count == 0)
            {
                return 0;
            }

            return lsBets.Count(a => a.WinAmount > 0) * 100 / lsBets.Count;
        }

    }
}
