﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WHATechChallenge.Main.Model;

namespace WHATechChallenge.Main
{
    public interface IBetFileReader
    {
        List<BetDetail> ParseBetDetails();
    }
}
