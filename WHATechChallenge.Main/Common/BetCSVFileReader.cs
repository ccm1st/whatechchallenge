﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WHATechChallenge.Main.Model;

namespace WHATechChallenge.Main.Common
{
    public class BetCSVFileReader : IBetFileReader
    {
        private readonly string _filePath;
        private readonly char _delimiter;

        public BetCSVFileReader(string filePath, char delimiter)
        {
            _filePath = filePath;
            _delimiter = delimiter;
        }

        public List<BetDetail> ParseBetDetails()
        {
            var bets = new List<BetDetail>();
            
            if (string.IsNullOrEmpty(_filePath) || !File.Exists(_filePath))
            {
                return bets;
            }

            var reader = new StreamReader(File.OpenRead(_filePath));
            
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                bets.Add(ReadRow(line));
            }

            return bets.Where(a => a != null).ToList();
        }

        private BetDetail ReadRow(string line)
        {
            var values = line.Split(_delimiter);

            if (!IsSchemaValid(values))
            {
                //log the row with invalid schema
                return null;
            }
   
            return new BetDetail
            {
                CustomerId = int.Parse(values[0]),
                EventId = int.Parse(values[1]),
                ParticipationId = int.Parse(values[2]),
                Stake = decimal.Parse(values[3]),
                WinAmount = decimal.Parse(values[4]),

            };
        }

        private bool IsSchemaValid(string[] values)
        {
            const int totalProperties = 5;
            int testIntValue;
            decimal testDecimalValue;
            if (values.Length != totalProperties)
            {
                return false;
            }

            return int.TryParse(values[0], out testIntValue)
                   && int.TryParse(values[1], out testIntValue)
                   && int.TryParse(values[2], out testIntValue)
                   && decimal.TryParse(values[3], out testDecimalValue)
                   && decimal.TryParse(values[4], out testDecimalValue);
        }
    }
}
