* The solution is built using VS2012
* The application is build in console, to run the project simple run the executable
* All output results are displayed in console window 
* Tests are been written in Nunit with help of Moq. If the test project fails to build, check the test project to make sure those packages have been restored correctly. 
* Exception Handling and logging is has not been include as part of the solution
